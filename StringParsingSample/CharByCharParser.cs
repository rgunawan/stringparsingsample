﻿using System.Collections.Generic;
using System.Text;

namespace StringParsingSample
{
    class CharByCharParser : IParser
    {

        public IEnumerable<Token> Parse(string input)
        {
            int cursor;
            var output = ParseInternal(input, out cursor);

            return output;
        }

        private IEnumerable<Token> ParseInternal(string input, out int endCursor)
        {
            var tokens = new List<Token>();

            if (string.IsNullOrWhiteSpace(input))
            {
                endCursor = 0;
                return tokens;
            }

            var cursor = 0;
            var isDoneWithTokens = false;
            var inputLength = input.Trim().Length;
            var textBuilder = new StringBuilder();

            while (cursor < inputLength && !isDoneWithTokens)
            {
                var curChar = input[cursor];
                if (curChar == '(')
                {
                    // start another child token list
                    int childCursor;
                    var childTokens = ParseInternal(input.Substring(cursor + 1), out childCursor);

                    if (textBuilder.Length == 0 && tokens.Count == 0)
                    {
                        // there's no text and tokens is empty, we're at the root.
                        tokens.AddRange(childTokens);
                    }
                    else
                        AddToTokenListAndClearBuilder(tokens, textBuilder, childTokens);

                    cursor += childCursor;  // the child tokens have been processed, go to the next token.
                }
                else if (curChar == ')')
                {
                    // we're done with child token list
                    AddToTokenListAndClearBuilder(tokens, textBuilder);
                    isDoneWithTokens = true;
                }
                else if (curChar == ',')
                {
                    // finish the current text
                    AddToTokenListAndClearBuilder(tokens, textBuilder);
                    textBuilder.Clear();
                }
                else
                {
                    textBuilder.Append(curChar);
                }

                cursor += 1;
            }

            if (textBuilder.Length > 0)
            {
                AddToTokenListAndClearBuilder(tokens, textBuilder);
            }

            endCursor = cursor;
            return tokens;
        }

        private static void AddToTokenListAndClearBuilder(List<Token> tokens, StringBuilder textBuilder, IEnumerable<Token> childTokens = null)
        {
            var text = textBuilder.ToString().Trim();
            if (!string.IsNullOrWhiteSpace(text) || childTokens != null)
            {
                tokens.Add(new Token(text, childTokens));
                textBuilder.Clear();
            }
        }
    }
}