﻿using System.Collections.Generic;

namespace StringParsingSample
{
    interface IParser
    {
        IEnumerable<Token> Parse(string input);
    }
}