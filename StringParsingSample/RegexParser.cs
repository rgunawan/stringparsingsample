﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StringParsingSample
{
    class RegexParser : IParser
    {
        public IEnumerable<Token> Parse(string input)
        {
            // this implementation uses regex to tokenize the input string.
            // the regex basically look for 2 things: a text part, and a children list inside parentheses.
            // e.g. text(child1, child2), text2, text3
            var tokens = new List<Token>();
            var regex = new Regex(@"(?<text>[a-z]+|)(\((?<child>[a-z\(, \)]+)\))?", RegexOptions.IgnoreCase);

            foreach (Match match in regex.Matches(input))
            {
                var textMatch = match.Groups["text"];
                var childMatch = match.Groups["child"];
                if (textMatch.Success && !string.IsNullOrWhiteSpace(textMatch.Value))
                {
                    // we have a text part. Let's see if we have child tokens.

                    if (childMatch.Success && !string.IsNullOrWhiteSpace(childMatch.Value))
                    {
                        // we do have child tokens, parse those and add to this token's child tokens list.
                        tokens.Add(new Token(textMatch.Value, Parse(childMatch.Value)));
                    }
                    else
                        tokens.Add(new Token(textMatch.Value));
                }
                else
                {
                    if (childMatch.Success && !string.IsNullOrWhiteSpace(childMatch.Value))
                    {
                        if (tokens.Any())
                        {
                            // no text token
                            tokens.Add(new Token("", Parse(childMatch.Value)));
                        }
                        else
                        {
                            // no text, but there're child tokens. And we don't have any tokens.
                            // the child tokens are the root list
                            tokens.AddRange(Parse(childMatch.Value));
                        }
                    }
                }
            }

            return tokens;
        }
    }
}