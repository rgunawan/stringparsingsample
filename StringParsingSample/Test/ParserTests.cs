﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringParsingSample.Test
{
    [TestClass]
    public abstract class ParserTests
    {
        internal abstract IParser GetTarget();

        [TestMethod]
        public void Parse_EmptyInput_EmptyOutput()
        {
            var target = GetTarget();

            var result = target.Parse("");

            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void Parse_EmptyParenthesesInput_EmptyOutput()
        {
            var target = GetTarget();

            var result = target.Parse("()");

            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void Parse_SingleItem_SingleObjectOutput()
        {
            var target = GetTarget();

            var result = target.Parse("(one)").ToList();

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("one", result.FirstOrDefault()?.Text);
        }

        [TestMethod]
        public void Parse_CommaDelimitedList_MultipleObjectsOutput()
        {
            var target = GetTarget();

            var result = target.Parse("(one,two,three)");

            Assert.IsNotNull(result);
            Assert.AreEqual("one,two,three", string.Join(",", result.Select(token => token.Text)));
        }

        [TestMethod]
        public void Parse_NestedList_NestedObjectsOutput()
        {
            var target = GetTarget();

            var result = target.Parse("(one(two,three))").ToList();

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("one", result[0]?.Text);
            
            var childTokens = result[0]?.Tokens.ToList();
            Assert.IsNotNull(childTokens);
            Assert.AreEqual(2, childTokens.Count);
            Assert.AreEqual("two", childTokens[0]?.Text);
            Assert.AreEqual("three", childTokens[1]?.Text);
        }

        [TestMethod]
        public void Parse_ChildTokensNoParentText_ListOutput()
        {
            var target = GetTarget();

            var result = target.Parse("one,(two,three),four").ToList();

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual("one", result[0]?.Text);
            Assert.AreEqual("", result[1]?.Text);
            Assert.AreEqual("four", result[2]?.Text);

            Assert.AreEqual("two,three", string.Join(",", result[1]?.Tokens?.Select(token => token.Text) ?? new string[0]));
        }

        [TestMethod]
        public void Parse_NoParenthesesInput_ListOutput()
        {
            var target = GetTarget();

            var result = target.Parse("one,two,three");

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count());
        }
    }

    [TestClass]
    public class CharByCharParserTests : ParserTests
    {
        #region Overrides of ParserTests

        internal override IParser GetTarget()
        {
            return new CharByCharParser();
        }

        #endregion
    }

    [TestClass]
    public class RegexParserTests : ParserTests
    {
        #region Overrides of ParserTests

        internal override IParser GetTarget()
        {
            return new RegexParser();
        }

        #endregion
    }
}
