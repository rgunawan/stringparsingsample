﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringParsingSample.Test
{
    [TestClass]
    public class ProgramTest
    {
        const string Input = "(id,created,employee(id,firstname,employeeType(id), lastname),location)";

        [TestMethod]
        public void StringParsingSampleParseTest()
        {
            var target = new Program();

            var result = target.Convert(Input);

            const string expected =
                "id\r\ncreated\r\nemployee\r\n- id\r\n- firstname\r\n- employeeType\r\n-- id\r\n- lastname\r\nlocation";

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void StringParsingSampleParseAndSortTest()
        {
            var target = new Program();

            var result = target.ConvertAndSort(Input);

            const string expected =
                "created\r\nemployee\r\n- employeeType\r\n-- id\r\n- firstname\r\n- id\r\n- lastname\r\nid\r\nlocation";

            Assert.AreEqual(expected, result);
        }
    }
}
