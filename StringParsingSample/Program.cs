﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StringParsingSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var prog = new Program();
            string output;

            if (args.Length == 0)
            {
                // handle "<exe name>"
                output = "usage: <exe name> <input string>\nor\nusage: <exe name> sort <input string>";
            }
            else if (args.Length == 2)
            {
                // handle "<exe name> sort <input string>
                output = prog.ConvertAndSort(args[1]);
            }
            else
            {
                // handle everything else
                output = prog.Convert(args.FirstOrDefault());
            }

            Console.WriteLine(output);
        }

        public string Convert(string input)
        {
            var tokens = Parse(input);

            return PrintTokens(tokens);
        }

        public string ConvertAndSort(string input)
        {
            var tokens = Parse(input);

            var sortedTokens = Sort(tokens);

            return PrintTokens(sortedTokens);
        }

        private IEnumerable<Token> Sort(IEnumerable<Token> input)
        {
            // sort the top level
            var sortedTokens = input.OrderBy(token => token.Text);

            // sort the children
            return sortedTokens.Select(token => new Token(token.Text, Sort(token.Tokens)));
        }

        private IEnumerable<Token> Parse(string input)
        {
            var parser = new RegexParser();
            return parser.Parse(input);
        }

        private string PrintTokens(IEnumerable<Token> tokens, int level = 0)
        {
            // figure out how many dashes we need to print
            var prefix = level == 0 ? string.Empty : new string('-', level) + ' ';

            // figure out each token's (and its descendants's) texts
            var tokenOutputs = tokens.Select(token =>
            {
                var outputBuilder = new StringBuilder();
                outputBuilder.Append(prefix);
                outputBuilder.Append(token.Text);

                if (token.Tokens.Any())
                {
                    var childTokenString = PrintTokens(token.Tokens, level + 1);
                    outputBuilder.Append(Environment.NewLine + childTokenString);
                }

                return outputBuilder.ToString();
            });

            // join the token texts together.
            return string.Join(Environment.NewLine, tokenOutputs);
        }
    }
}
