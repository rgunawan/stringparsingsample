﻿using System.Collections.Generic;

namespace StringParsingSample
{
    public class Token
    {
        public string Text { get; }
        public IEnumerable<Token> Tokens { get; }

        public Token(string text, IEnumerable<Token> tokens = null)
        {
            Text = text;
            Tokens = tokens ?? new List<Token>();
        }
    }
}