# String Parsing Example

An example of doing string parsing with C# using:
* Reading input character by character 
* Using Regex
* TODO: Using a [BNF](https://en.wikipedia.org/wiki/Backus%E2%80%93Naur_form) parser (e.g. [Eto.Parse](https://github.com/picoe/Eto.Parse))

This console apps gets its input from the command line.
It parses a comma-separated list of texts and puts them into separate lines.
It supports sub-list, which are comma-separated lists inside a set of parentheses next to a list item.

## Input/Output Example

__Input__: one,two(sub-two,sub-three),four

Output:

~~~~
one
two
| sub-two
| sub-three
four
~~~~

